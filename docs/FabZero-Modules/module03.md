# 3.1 3D printing

## General notes about additive manufacturing
One of the key manufacturing process in a fablab is the availability of 3D printers, which have gotten increasingly cheap and available even for highly precise printing. The advantages of additive manufacturing is it's capacity of creating highly complex parts with fast iteration cycles on the design, as well as the ability to have quite a variety of materials to work with. Only downside is that first most of the created pieces might need surface treatment, and if the need of passing to a larger scale manufacturing it shows great challenges even with a lot of printers available in a farm printing setting. It is however perfect for fast prototyping and complex shapes.

## Basis of FDM 3D printing
The two 3D printing methods readily available to the consumer are Fused Deposition Modeling (FDM) and now Stereolithography (SLA). In the first case hot spots of molten plastic is deposited on top of each other from a nozzle, building the part numerically. In the case of SLA, we use photosensible resins in a bath, carving (burning) the shape layer per layer with either a laser, or high quality super-bright display. SLA are more expensive, and less flexible regarding the choice of materials, but give better surfaces in the end-products.

## The slicer
The most important software associated to the printer is the *slicer*, as the word says it slices the ```.stl``` file from any 3D modeling software (OpenScad, Freecad, Fusion360, etc), then slices it in layers as instructions for the printer. The quality of the print depends heavily on the printing parameters, which have to be fined tuned with the chosen materials. To name the most important ones : printing speed, temperature (which is default depending on the materials that has to be properly chosen), skirt, manual adjustment of the support structures.

In our case, the opensource [prusaslicer](https://www.prusa3d.fr/prusaslicer/) will do the job perfectly, in fact ```gcode``` the standard output file of the slicer with all encoded printer parameters is standard through the industry and presets for most of 3D printers manufacturers are available on the prusaslicer. Usually 3D printing conception works as follows :

![img](images/cycle_printing.jpg)

### Working around the prusaslicer
A lot of different slicers programs are available either for free or paid with certain models of printers. In the fablab we have to our disposal the Prusa Slicer, build for the Prusa brand of printers however are compatible with a large array of printers. One should however keep in mind that a certain set of settings should only be used for the particular printer you are going to use, for the simple reason that heat beds and frame sizes vary according to the printer model.

![prusaimage](images/Prusa.png)

The ```.stl``` files are imported in the slicer and can be placed anywhere on the plate. On the right various tools for placing the piece on any of its faces. On this example it is clear that the piece should be rotated on its side.

The box on the top right might be the most important parameters one should look into before exporting gcode. The quality will determine the ```print settings``` will determine the resolution of the print, and thus will influence heavily the print time with the volume of the part. ```filament``` menu has to be the right materials you have because it will determine the optimal fan power and extruder temperature which is material dependent.

Looking at the advance print settings on the top of the window we fine tune the slicer parameters :

![advance_settings](images/slicer_advance.jpg)

This is where the core of the fine-tuning is happening. Prusaslicer automatically manages default temperatures variations when the plastic is cooling and deforming, which is a nice feature to have. Getting into particular details of each functionality might be a waste of time, but let's see the most important elements.

**Infill** allows you to to alter internal structure of your prints. You have to remember that 3D printed parts are largely hollow, the infill density and geometry which you can choose will largely influence the mechanical properties of your part.

**First layer** is the most important layer of the print, it must stick to the bed but more than that it is where the most distortion happens. This is why Prusaslicer gives you more options to increase resolution for the basis to limit those effects. You might have to do a few tries to have exactly what is required depending on the expected tolerances.

**Skirt and brim** Draws region where the print is going to happen, if your first layer is not sufficient don't be afraid of adding skirt and brim. Better having to remove a first layer than a broken layer ! (if for example, first layer corrections are not sufficient, or first layer deformations)

**Support material** is essential for complicated parts. Easy to remove material to hold hanging parts of your print. Just pay attention to not overdo the support material density.


#### Getting around various print parameters
When parameters seems sufficient, pressing ```Slice now``` on the bottom right gives us a preview of the sliced part. With the slider on the middle of the screen we can see how the printer is going to add layers by layers to build the print. Varying colors gives information about the printer speed, support materials are shown in another color as well.

At this point if satisfied with all parameters and slice. It is time to export gcode. Nothing easier, the exported gcode is named after the part and estimated time for print. The only thing left to do is to try, then come back to the slicer for improving the print.

## Prototyping and adjusting printing parameters
Of course, our flexlinks are not going to be perfect right away. On my first print the dimensioning of the Lego holes was 0.1mm off, after correcting it fits perfectly into the lego pieces. Working with the Prusa slicer. We get

![flex1](images/flexlinks.jpg)
![flex2](images/gage.jpg)

In any case, one should keep in mind that 3D printers are becoming very good at surfaces, but that due to the nature of the process the mechanical properties of 3D printed parts are heavily anisotropic. When printing please **think about how the part is going to absorb constraints.** It is absolutely essential as the fibers from the printer, layered above each other will be very strong in the parallel axis of the layers and be weaker on the perpendicular axis.

# 3.2 Building a set of Flexlinks
From the set we build together (with my curved designs, multiple and from the others : clips etc...), as well as my experience with my own cheat printer, I compile a list of troubleshoots in case of printer failure. :


* **Bed levelling** : This is the champion of the cheap printers. The Prusa Mk3 has electronic sensor system to get that for you, however it is not the case for most printers. If you have troubles getting the part to stick, or with nozzle collision with the piece, it is often the first and easiest thing to check.

* **Proper nozzle and heatbed temperature** : be very wary of what filament you use. PLA has a different working printing temperature from other materials such as nylon.

* **Bad filament** : sometimes PLA can absorb humidity, a cheap off the shelve filament which has not been properly sealed and packaged will bare bad results.

* **Bad adherence for glass heatbeds** most of the time, using a bed made out of glass can help tremendously the leveling problems. However it has no texture for the basis of the part to hold onto. Special glues are available to deal with this, it can be done without glue, but in that case a lot of fidling around with temperatures specs will be required.

* **Impossible to print designs** Most of the time, very complicated designs will be easily printable, depending on how smart you were with the part orientation in the slicer. Please waste no space.

## Printing the whole set :

One of the best way to deal with multiple pieces is to try to place them efficiently on the plate. This is merely one example with various curvatures, simple spring and double spring. Brims and skirts can be useful but not always mandatory, support materials are generally useful.

![](images/PrusaSlicer-group.jpg)

# Demonstration
