# 6. Prototyping electronics : PCB soldering and design


## Part 2 : Electronic soldering and microchip design
In the previous section we prototyped a small function for the microchip, breadboarding is however not suited for use purpose, fragile and not compact enough. Of course, in most application one can design its own PCB chip, on which micro-electronics components are soldered.

For this purpose, an exercise has been prepared for us, quite a detailed tutorial was written for us as well, in that case let's review the soldering process, and what to watch for.

**protips on soldering**
The first time you do electronics soldering it is, simply put, a bobbling mess. Tin gets everywhere like sand, and who likes sand. First of all you have two main types of soldering irons : gas ones, which are cheaper but need to be manually re-heated, without knowing what temperature the tip is and electric soldering irons which can (mostly) be configured.

1. *Keep the tip of the iron clean*
2. *Go slow, but not too slow*
3. *Tin droplets need to stick to the tip, if not cleaning is necessary*
4. *PCB is designed* to stick tin where it should be, thus most of the time we do not need to have surgeon hands, tin sticks to the copper and not well in the grooves where it creates short circuit.
5. *In case of failure don"t panik and use the desoldering pump*
6. *Micro-electronics components can easily fry, don't touch them with the tip*

<table><tr>
<td> <img src="../images/soldering1.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/soldering2.jpg" style="width: 300px;"/> </td>
</tr></table>

In this exercise we wish to soldier and program the [development board](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/dd760ea3b49b74d278b1eec1e4be9e5a45a986a6/Make-Your-Own-Arduino.md), based on the popular [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) from the explanations available. This is quite long, but with a little help it is finally recognized by plugging it in the USB port. My first attempt was not successful, and I had the chance to have another go. Once the basis of the programmable chip is applied on it is always good to check if everything is working (thus in our case, if the chip is detected by the OS of the computer). In troubleshooting it is ok to place too much of tin, as long as the pump is well used.


<table><tr>
<td> <img src="../images/notfinishedPCB.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/connectedPCB.jpg" style="width: 300px;"/> </td>
</tr></table>


 On Linux you can list all available USB busses with :


```bash
lsusb
```

It is however not very talkative about what hardware it is using except the brand, we'd rather use

```bash
dmesg
```

Displays well the brand new device with information about it :

![](../images/usbdevice.jpg)

Once the board is finished according to specifications, it can be plugged, and elements such as the LED control can be programmed. On a practical note, the SAMD11C can also be programmed using the Arduino IDE, which only has to be configured for this chip in the menus. We can make the LED blink the same way we programmed the Arduino. The additional pins allows for additional controls on numerical and if the case may be analog pins.

This PCB design is USB compatible, however in most of the time, you either program the microcontroller flashed on Arduino then soldier it on the PCB, or more logically, program it by using a USB to pins connector. On this board we added a button, so let's use it.

As the start of the microcontroller, we simply implement a global state variable and listen to the pin switch. When the switch is pressed the short-circuit changes the pin state which we record to -for example- change the blinking frequency. We can also turn the LED on and off, of course. For example a small program would :

```C

/*
   Authors : Théo Lisart
   License : GPL3

   Blinks the LED at varying frequency when the button is pushed
 */

// Define general variables, could be done more elegantly with #DEFINE
int LEDBUS = 15;
int BUTT = 8;

int UP_FREQ = 500;
int DOWN_FREQ = 1500;


void setup() {
  pinMode(LEDBUS, OUTPUT); // _Inti_ LED bus
  pinMode(BUTT, INPUT);    // _Init_ Button listener
}

void loop() {
  if(digitalRead(BUTT) == 0){     // Checks the button state
    digitalWrite(LEDBUS, HIGH);   // blinks LED at
    delay(UP_FREQ);
    digitalWrite(LEDBUS, LOW);    // blinks LED at
    delay(UP_FREQ);
  }

  else {
    digitalWrite(LEDBUS, HIGH);   // blinks LED at
    delay(DOWN_FREQ);
    digitalWrite(LEDBUS, LOW);    // blinks LED at
    delay(UP_FREQ);
  }
}
```

Where the pins number where everything is connected is of course design dependent which are specified on the microcontroller datasheet as well as the PCB datasheet depending on the needs.

**Some hints to myself on pathmaking**
##### KiCAD, on automation and the arcane art of making PCB
Tools exist to help you design your PCB, but it is still an art and everything is worth spending a little time on. The standard technique is using quite a big software : [kiCAD](https://kicad.org/) where you place all your components on the board, then manually or programmatically link all of them, then with sophisticated algorithms the software find the near-optimal routing which you can improve upon. In an easier way, fierce workers at fablab ULB build [Frep-editor](https://fab.cba.mit.edu/classes/865.21/projects/frep-editor/?fbclid=IwAR1Tfb77JPQDCh5A9NNp_OKld0uEfh2ZIrlYqt1uRj0zAPF665HRq_-4q2o) to make this kind of approach more accessible (it is however lacking documentation at the moment). In any case having *some* practice in creating those kind of design is interesting. I just wanted to name it (here the workbench is very dark, my theme's fault).

![](../images/kicad.jpg)

A very interesting and new way of producing PCB is through PNG top mill conversion. Using again [Frep-editor](https://fab.cba.mit.edu/classes/865.21/projects/frep-editor/?fbclid=IwAR1Tfb77JPQDCh5A9NNp_OKld0uEfh2ZIrlYqt1uRj0zAPF665HRq_-4q2o), one can simply draw a PNG picture of the micro-electronics components sockets, then using [PNG to milling](http://mods.cba.mit.edu/) (programs -> Open server programs -> Mill from PNG). The very interesting thing with that program is that through node programming one can really see what the image processing system does.

However that's not it. Once the pseudo-gcode is build in the ```.nc``` format, one should always do a quick look at the CNC instructions on the file. In Fablab ULB we have a Bantam PCB, which is good, but unfortunately uses proprietary software.

## Milling the PCB at home (aka with Fablab machinery)
For simplicity's sake and demonstration's sake, let's take a known design from Fabacademy and [Neil's designs](http://academy.cba.mit.edu/classes/embedded_programming/index.html?fbclid=IwAR3VXcklrg8h6jbaJRpa2udDw0-heniS2G74AQLrOGsDOwNPP8fNm7TgZA0), we'll document how to use it for our purpose.

The first step is to use the current detection from the mill to determine the zero point in the z direction, then we can do the same from the bed to compute the overall thickness of the PCB piece. At this point we can import our ```.nc``` and from the mill software execute multiple pass depending on what we want to make as PCB. In this test we tried naked PCB with socket only.

Starting from the thinnest mill, we go up to remove materials step by step, watching closely the path the mill is going to take on the visualizer. For illustration's sake :

The machine
<table><tr>
<td> <img src="../images/mill4.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/mill1.jpg" style="width: 300px;"/> </td>
</tr></table>

The milling tips
<table><tr>
<td> <img src="../images/mill2.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/mill3.jpg" style="width: 300px;"/> </td>
</tr></table>

The result, after the first thickness, then the cleaning up of all materials.
<table><tr>
<td> <img src="../images/mill5.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/mill6.jpg" style="width: 300px;"/> </td>
</tr></table>
