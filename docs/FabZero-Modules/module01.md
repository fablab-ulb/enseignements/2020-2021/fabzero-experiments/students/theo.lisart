# 1. Project management and documentation

Let us start our journey through full-stack design, as somewhat of a to-become frugal design *expert* let us review our tools before doing anything illegal (to mathematicians). So, let's try first to understand why is the hacking community so obsess with making a personal webpages to communicate, then why is the programming community so obsess with version management software, so-called ```git``` and how to use it efficiently. Finally, why on earth do we need any markup coding to express ourselves clearly when we could just write some messages down on a word file that we pass around ?

### The power of static web-design, how ?
First of all, what is a static website ? Well a static website is the contrary to a dynamic website. Thanks Sherlock, very helpful, well it is a webpage that is going to display itself the same to every users, that is no need for fancy database management, no need for a burst of tools and javascript server-side handling. In fact, it is the easiest way of making a webpage. It is the most suited way of building a page for personal use in fact, and many tools have been developed for users to build pages without touching a single line of code (except minor tweaking). Here, we are using ```Mkdocs```, it is a dead simple and easy to use static website generator written in python. Other methods consist in for example ```Jekyll``` in ruby or to name a few, ```Hugo``` or ```Gatsby```. They all have their own functionalities and advantages : Mkdocs is build to create fast and well thought-out documentation for technical projects, Jekyll is more blog-oriented, etc...

The cool thing about making a website for personal use and managing documentation (or both), is that the absolute flexibility contrary to making a page for example exclusively on ```github``` or ```gitlab``` (we'll come back to that). Everything can be properly searched, layout according to the creator expectations and a way to the creator to express itself in a deeper way. Somewhat with a little effort (very low), everything that is displayed has been thought thorough. This gives personality and actually helps _getting_ a project or person. In a project such as ```FabZero```, the community aspect of OpenSource is absolutely essential, especially to deploy the sheer manpower of passionate creators, makers and engineers that will be here to help you and find your place. All of it is made easier when reading through development journal that feels like it has been written by someone and not by some kind of automated process. Of course, pure documentation should be written accordingly. This is all in finding the balance between transmitting passion for a subject or interest and making the text technically sound and clear.

### Why are you writing this doc ? To whom, and for what purpose ?
So you chose the static website generator that sounded like your purpose, and you launched the local server. The default design is popping up and everything works properly. Of course it did, you followed the documentation properly on their website !

Like us, they build something and explained on their website how it works, they knew that people coming over for a static website generator wanted it to work as fast as possible and they wrote their documentation for that exact purpose. From the start of writing documentation one should ask the questions of who's going to read it ?

Here for example :

* **Who** : students enrolled in FabZero course stuck in their own work, looking for some inspiration. People interested in my production during the course from the maker community, or students interested in one specific tutorial.

* **Why** : reproducing what I did, or getting inspired by what I did ? Both are very different. Being interested in the process or in the production are two radically different styles. Depending on the project outcome the layout of information should be adapted. Another reason to write would be a personal journal to keep track of versions and how to do X or Y during the project. Like a very public and elaborate memo. Once again, we never know something interesting could be useful to someone else.

Give reasons for others to help you, like helping other. Help on the internet is never due.

### Version management, why ?
Programs or any virtualized project can get big. In fact, it can get horrendously big. Hundreds of people can end up working on the same codebase which is a nightmare to manage. The solution is using version management software : it tracks each version that have been **pushed**, allows to come back to changes, and most importantly makes way easier comparison between modifications from various versions and various people.

**But I am working alone ! Why should I do this ? It seems like overly complicating the workflow !** First of all, it's not that hard once you have tried a few times, second of all, it is very useful when working alone too !

* You can experiment with your project varying experimental versions easily.
* You can share easily the codebase if you need help.
* You have cloud redundancy if your computer explodes or is stolen.
* You get to look like an awesome hacker using command lines.
* ... etc

Also important thing to note : **```git``` is the version management softwate**, ```github``` and ```gitlab``` are **fancy hosting services** you absolutely can use git locally only, or make your own remote ```repository``` on your own server. The service that gitlab/hub are proposing is some server space with version management software ready to go. It is a little more than that as it also propose group working (with issues, incidents, messages) and ```continuous integration```, allowing you to build complete server-side web applications.

The interesting thing, is that in those services gitlab/hub respectively propose pages integration. That means that they are already equipped with tools to understand the static website generator code you just used so you don't have to build it yourself. It does it (almost) automatically on its own. This avoids us a lot of pain.

So here's the question : how to use ```git``` ? Well, basic git usage is not complicated from anyone having used terminal-based applications at least once. The main commands are :


```bash
git pull
```
Checks for remote conflicts, download latest deltas from the selected branch

```bash
git add *
```
Adds all new files which are not too big, or hidden folders denoted by ```_```, for example ```_site``` from your generated site will not be added to the new commit.

```bash
git commit -m "Latest commit, I should always explain what I did modifying"
```
The commit is the ID card of the last package of modifications. Git records this with timestamp and associated commit message or logs.

When you start experimenting with the code and you are not sure you will want to keep changes (or you don't want other to interfere with it ofr example), you should create a branch and select it. When you beleive the last version of the branch is now functional and ready to "ship", you call the ```merge``` command, to well, merge branches (either together or with the master/origin branch)

```bash
git checkout -b my_new_branch
```

This command concatenates two command :

```bash
git branch my_new_branch
git checkout my_new_branch
```

You can list all remote and local branches by

```brash
git branch -a
```

See documentation for all flags suited for your needs. This is far from an exhaustive list.

### Markup VS programming, what for ?
So you are wondering why everyone is forcing you to use ```markdown``` as markup language for your static website but you are wondering "why not just use word format ?", or "why should I know the very basis of HTML I am not a webdev !". Well, markup languages, born in the prehistoric times of when actual type-setting was a nightmare [(see prof Brailsford ramble about it)](https://www.youtube.com/watch?v=RH0o-QjnwDg). There's an interpreted markup language for everything it seems : basic one as markdown, BBcode for the forums nostalgics, Rmarkdown for data typesettings, \( \LaTeX \) for mathematics, etc...

The thing is, all markup applications follows the same idea as their grandfather SGML, Standard Generalized Markup Language that gave birth to XML. The power of XML is that as SGML it can be internally re-defined. HTML is -obviously- the most famous and used XML application !

In fact, this markup philosophy is even more general and why it is useful anyways, at least to read properly. In [Module 4](module04.md) we will see for example how the ```SVG``` format is in fact some application of XML.

### Frugal design : fast iteration and PM
Frugal design is, simply put, a purely design-oriented philosophy based upon building tools and technical solutions by being the simplest, and the most frugal possible. In fact, this concept which originated in the USA has been extended in France around the low-tech movement, adding social aspects : right to repair, and ethical reflection about some or some technological deployment. The latter is more an idea around technologies and private property : where should high-techs be deployed (where the upsides and social impact outmatches production downsides on the entire life-cycle of a technology) ?

Here we will focus on frugal design, which often automatically implies when mixed with free/opensource social impact by definition through access to information.

In frugal design simple is good. The approach is problem-based. Taking one problem we wish to solve or improve, we look for small changes and applications where in actuality adding constraints brings novelty in design. This is why this is a fast-iteration based process : getting basic concepts to work as fast as possible, then adding things and improvements pieces per pieces.
