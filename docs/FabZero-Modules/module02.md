# 2. Numerical conception : babysteps
Rome was not build in a night, neither is 3D conception skills. In those lectures we saw an overview of libre software used in digital fabrication, namely OpenSCAD and FreeCAD. My curiosity for the SCAD method was right away titillated as I don't like to draw, except when it's programming.

## Learning by doing : [Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks) clones
[Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks) are small structures for building kinematic systems based on lego bricks. This exercice is simply build a 3D model describing the object numerically. The usual approach is building the basis of the structure parametrically using scad software (thus programming based design), then export the file into a CAD software for improvement and correction. The clear advantage in this method is that one can generalize a basic idea for building a whole class of one type of object, reducing redundancy if we want to build similar objects.

[Link to the project](https://gitlab.com/theo.lisart/parametric_flexlinks)

### Basis of OpenSCAD
In OpenSCAD one programs parts in pseudo-C non-typed language proper to the software. One can define variables or work with strings and basic procedural programming logic (for loops, if's, else). Most of the actions which can be taken can be found on the [official cheat sheet](https://www.openscad.org/cheatsheet/).

On principle the process in pretty simple : using basic shapes volumes, shapes on shapes operations, and 2D shapes extrusion one can build algorithmically simple or complex parts. The power of the tool lays in the object-oriented approach : by building simple sub-parts on ```modules``` we can create a large quantity of variant.

Let's see the code we get for the gage :

```C
// Redering parameters
$fn = 300;   // Division


module gage(hole_amount, hole_radius, depth, width, height, orientation){

    /*
        Gage module defining the geometries of the flexgage
        Note : this could be obviously written without code redundancy, and should be written
               without redoundancy.
    */

    // Drawing --------------------------------------------------------------------------------------------------------

    height_cyl = height;
    radius_cyl = depth/2;

    total_length = 2*radius_cyl + width;
    offset = total_length/hole_amount;
    epsilon = 0.1;

    // Taking the union of the cube and a cylinder at both extremities of the cube

    if(orientation == "vert"){
    rotate(90, [1, 0, 0])

        difference(){
            // basis
            union(){
                cube([width,depth,height], center=true);
                translate(v = [width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
                translate(v = [-width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
            }
            // Boring holes
            for (i = [0 : hole_amount]){
                translate(v = [width/2 - i*offset, 0, 0])
                cylinder(h = height_cyl + epsilon, r = hole_radius, center = true);
            }
        }
    }

    else if(orientation == "horiz"){
        difference(){
            // basis
            union(){
                cube([width,depth,height], center=true);
                translate(v = [width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
                translate(v = [-width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
            }
            // Boring holes
            for (i = [0 : hole_amount]){
                translate(v = [width/2 - i*offset, 0, 0])
                cylinder(h = height_cyl + epsilon, r = hole_radius, center = true);
            }
        }
    }
}
```

First, there must be a way to write this without the code redundancy, on the if loop rotating the part 90 degree along the x axis, but as rotations work as operators (thus no ```;``` at the end of the rotation) there's no obvious may to isolate the rotation command.

Then one realize how we should think logically the drawing, this is a bottom-up approach, that means that starting from the simplest shape we add transforms over the initial shape. Looking at :

```C
difference(){
    // basis
    union(){
        cube([width,depth,height], center=true);
        translate(v = [width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
        translate(v = [-width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
    }
    // Boring holes
    for (i = [0 : hole_amount]){
        translate(v = [width/2 - i*offset, 0, 0])
        cylinder(h = height_cyl + epsilon, r = hole_radius, center = true);
    }
}
```

We started with the ```cube(arg[])``` call, then we created the two cylinders ```cylinder()```, which we translated to the end of the parallelepiped. All objects in the ```union(){}``` command are jointed as one object. Finally we bore the holes from the flexlink design as a for loop to insert as much holes as needed in the parametric design using the ```difference(){}``` command. Easy enough, the first object is cut of all following objects.

For now, the beam between two gages is going to be a simple parallepiped, in is own module also. A relatively simple improvement would be using the extrusion tool from a rectangle to give it a curved shape. The complete simple clone is on the gitlab page. We get :

![image](images/3D_view.jpg)

Let's think about a structure that we can build based on this simple concept that can actually be fun to play with (based on the principle that we want to use the flexibility properties of the material to play with legos). Exploiting as much as possible the ```module``` concept.



Rotations around a center extremity      |  Rotation around the beam center with extra axes
:-------------------------:|:-------------------------:
<img src= "../images/complex1.jpg" width="400"> | <img src= "../images/complex2.jpg" width="500"/>


### Going further : curved structure using extruding

Using some basic trigonometry one can build using the ```rotate_extrude``` function curved beams with arbitrary length branches. After tinkering a little with translations and measurment along curved beams we have (code available on the gitlab page):


Rotations around a center extremity      |  Rotation around the beam center with extra axes
:-------------------------:|:-------------------------:
<img src= "../images/curved.jpg" width="600"> | <img src= "../images/curved_gage.jpg" width="500"/>

The interesting thing with this method is that we build the entire class of any two-sided one inflexion point Flexlink by iteratively building upon previous models. The attentive eye sees the Unix ideas an suckless ideas of software, where we expect programs to be as small as possible to build more complex things with the certainty than the small pieces are optimized.

Nothing stops from mixing the curved design with the rotation design, or writing the curved flexlink as module as done previously to create more complex structures.

We'll stay there at the moment, but those ideas will be expended upon on the next module to build a complete set of compliant designs. 
