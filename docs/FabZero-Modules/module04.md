# 4. Numerical cutting

Numerical cutting such as laser cutters and vinyl cutters are one essential tools in any fablab, and will come handy well... for cutting (and engraving). Unfortunately each of the techniques have inconvenient : laser cutters burn easily their targets, can't cut reflective materials such as metals and vinyl cutters are way more effective at cutting fabric, paper and plastics but are not very good at anything else and very sensitive machines (that as my heart, breaks easily).

My generative [SVG parser](https://gitlab.com/theo.lisart/vector-kirigami-parser), which is very drafty at the moment but feel free to contribute !


## Setting the stage : calibrating the laser cutter
Standard approach to calibrate the laser cutter is using a template file by varying speed and power. For example to cut paper we build the file :

![svgfile](images/Reduit_couleurs_calib.svg)

Of course you can use it ! For any type of plexiglas or woods the same kind of files should be used before doing anything. Colors are used to specify to the printer different cutting or engraving settings. For some reason the display of the image is cut, it should look normal on the printer software.
## Example target : bi-stable materials

Bi-stable materials are very interesting creatures, when you are deforming the material on one axis, all axis are deformed outward, this is the magic of geometry.


To understand what we are dealing with, let's look at three papers : [Extreme mechanics letters](https://www.journals.elsevier.com/extreme-mechanics-letters), [Mechanical metamaterials with star-shaped pores exhibiting negative and zero Poisson's ratio](https://www.sciencedirect.com/science/article/abs/pii/S0264127518301424) and (wait for it) [Design of cut unit geometry in hierarchical kirigami-based auxetic metamaterials for high stretchability and compressibility](https://www.sciencedirect.com/science/article/pii/S235243161630058X) which is a mouth full, but at least it's not [ Friends with benefits: on the positive consequences of pet ownership ](https://pubmed.ncbi.nlm.nih.gov/21728449/) or either [ Ashes to ashes: thermal contact burns in children caused by recreational fires ](https://pubmed.ncbi.nlm.nih.gov/18789590/) which have fortunately nothing to do with design except title designs.

So as we explained in the previous section, laser cutters pretty much works like a printer. You give it vector parameters at which you define the output power of the laser, as well as the speed of the laser passing though. So let's build an easy way of parameterize bi-stable materials.

## Understanding vector formats for generative design
Two ways of getting the laser printer to engrave is through *bitmaps* or most vector curved drawings such as ```AI, EPS, SVG, DXF, DWG, PDF``` however the pdf format is probably never used in this context. This allows for quite a lot of flexibility. In our context we will focus on ```.svg``` as it is the most often used and easiest to handle in my opinion. Let's look at what is a .svg, and thus how to view the file and write such a file from scratch to be able to do generative drawing. This has two advantages : we will be able to do parametric design in one hand, on the other hand it scales better when we know exactly how original files were generated.

The format is mostly used for logo and icons, as its name says : **Scalable Vector Graphics**. In the file it gives all geometric information about the logo (with alphas for transparency, strokes and styles). Let's draw a svg file in *Inkskape* and let's see what it is all about, in any image viewer the svg looks like the following, however by opening the file in any text editor one gets :

![circle](images/circle.png)


```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="210mm"
   height="297mm"
   viewBox="0 0 210 297"
   version="1.1"
   id="svg8"
   inkscape:version="0.92.3 (2405546, 2018-03-11)"
   sodipodi:docname="circle.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="0.7"
     inkscape:cx="467.11801"
     inkscape:cy="538.14755"
     inkscape:document-units="mm"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1920"
     inkscape:window-height="1056"
     inkscape:window-x="0"
     inkscape:window-y="0"
     inkscape:window-maximized="0" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1">
    <circle
       id="path10"
       cx="85.989586"
       cy="104.04315"
       r="21.733631"
       style="stroke-width:0.26458332" />
  </g>
</svg>
```
This is rather long for a small circle, however a lot of this is bloat created by inkscape. The first thing to note is that svg is nothing else thant a subclass of XML, a general markup language. Two fields are of importance : the ```<svg></svg>``` field and in addition the header file

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
```

When we are going to be generating our codes, this has to stay at the top of the file, as it tells the interpreter in what standard the file is written. The second most important part is :


```xml
<g
   inkscape:label="Layer 1"
   inkscape:groupmode="layer"
   id="layer1">
  <circle
     id="path10"
     cx="85.989586"
     cy="104.04315"
     r="21.733631"
     style="stroke-width:0.26458332" />
</g>
```

Giving first the circle command, then the axis length (every circles are ellipsis), radius and stroke, amount of layers. This code is too dense, let's draw a circle by hand to know what is the bare minimum needed to draw a circle in this formal, this is going to be our basis to go forward.

Before doing anything useless, let's keep in mind that in the laser cutter case, stroke thickness can be detrimental, as the actual size of the cuts are limited by the laser beam cross-section. We will fix any line strokes at 0.1 (quantity that worked in practice at fablab).

People already know how bloated .svg files can get, and thus developed [svg minifiers](https://www.svgminify.com/). Our previous code writes :

```xml

<?xml version="1.0" encoding="UTF-8"?>
<svg width="210mm" height="297mm" version="1.1" viewBox="0 0 210 297" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<metadata>
<rdf:RDF>
<cc:Work rdf:about="">
<dc:format>image/svg+xml</dc:format>
<dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
<dc:title/>
</cc:Work>
</rdf:RDF>
</metadata>
<circle cx="85.99" cy="104.04" r="21.734" stroke-width=".26458"/>
</svg>
```

Which is miles easier to read. So we need to write based on this file a parser which is going to add to the file what is required. We'll keep the metadata information, but in that case we can add for example in the meta, authors, dates of production, etc...

In designing our svg parser, my habits would be to directly write some C script with output-input to be able to use linux pipes to the fullest. But this is kept for a tutorial later on, the code will be close to same so let's write this in python (the logical choice should be javascript to implement a small web-app, but I don't like javascript and Python interfaces really well in web applications).

#### Initial strategy : creating the file line per lines.
XML is nothing else than markup text file with ```.svg``` extention added to it. As a warning it is important to note that the method I am going to use is absolutely not the most efficient way to do this. Please feel free to write your own parser which uses flags to change and add elements to the text file at the right places. This is what should be done.

[Link to the project](https://gitlab.com/theo.lisart/vector-kirigami-parser)

#### We only need lines, so let's generalize lines, angles and distances
If you absolutely need to do algorithmic design into graphical images the most sensible idea would be using great tools that already exist (2D mode in for example [Antimony](https://www.mattkeeter.com/projects/antimony/3/)). However I am from the philosophy of thinking that you should build some of your tools at least for learning purpose. The cool thing is from 2D vectors rotating them in 2D space is the most simple matricial operation out there.

**Drawing a line**

```python
def line(pos_x1, pos_y1, pos_x2, pos_y2, stroke, rgb) :

    """
    Returns the string encoding of the XML line
    Color parameter is included, as in the 3D printer color can define various
    laser parameters

    pos_x1 = floating point - initial point
    pos_y1 = floating point - initial point
    pos_x2 = floating point - arrival point
    pos_y2 = floating point - arrival point

    stroke = floating point (should be generally set)
    rgb = [R, G, B], int[3] array
    """

    line_string = "<line x1=\""+ str(pos_x1) + "\" y1=\""+ str(pos_y1) + "\" x2=\"" + str(pos_x2) + "\" y2=\"" + str(pos_y2) + "\" style=\"stroke:rgb(" + str(rgb[0]) +","+str(rgb[1]) + ","+str(rgb[2]) + ")" + ";stroke-width:" + str(stroke) + "\" />"
    return line_string
```

This one is straight forward, give it initial point, final point, it concatenates the string into a formatted XML parametric line. This method can be generalized to any curve, it however involves a little more understanding of the ```SVG``` format.

**Our first design** is going to be the most simple kirigami : a set of horizontal lines where the structure is going to deform. The algorithm is simple enough and always based on the basic-cell structure which is going to be helpful going further.

```python
for i in range(cells_x) :
    for j in range(cells_y) :

        # Horizontal lines
        if(j%2 == 0):
            line_center_horiz= (i*x_offset, y_offset + j*y_offset, x_offset - cut_space/2 + i*x_offset, y_offset + j*y_offset)
            cell_geometry.append(line_center_horiz)

        elif(j%2==1) :
            line_center_horiz= (i*x_offset + x_offset/2, y_offset + j*y_offset, x_offset + x_offset/2 - cut_space/2 + i*x_offset, y_offset + j*y_offset)
            cell_geometry.append(line_center_horiz)
```
(for more information on how it works do not hesitate to check the code yourself)

Gives :
![pattern1](images/pattern1.jpg)

The basic idea is there, let's try to do something a little more advance.

## Designing bistable structures
Now that wa have a structure to do in fact any lines or any generative design. Let's build the *level 1 design* from the *extreme mechanical letters* publication. This design is in fact nothing more than a slight evolution of the previous design. Let's use symmetries to imitate parametrically one of the known bi-stable structures from the previous papers.

The only thing we need to be able to do are basic transformations : *translate*, *rotate* and *mirror* along the x and y axis. as reminder we know that mathematically all of them can be seen as matrices.


```python
def rotate(line, angle) :
    """
    Rotate a line as vector in the 2D space, attention to the fact that the y axes
    points downward.

    simple matvec operation

     _                    _
    |cos(theta) -sin(theta)|
    |sin(theta)  cos(theta)|
    -                      -

    All input vectors are supposed to be at the origin. A generalization is easy
    to implement but requires extra translations.
    """

    vec = (line[2], line[3])
    return (0, 0, vec[0]*m.cos(angle) + vec[1]*m.sin(angle), -vec[0]*m.sin(angle)+ vec[1]*m.cos(angle))
```
Easily enough, any line defined as a quadruple of points will have all its points rotated around the origin axis (the upper left corner of the image)

Translation is straight-forward and a flip writes easily enough :

```python
def flip_x(all_lines) :
    """
    Returns a list of lines of the flipped along default x axis
    Apply bitflip : U = [[1, 0], [0, -1]]
    """

    new_line_list = []

    for i in range(len(all_lines)) :
        buffer_line = (all_lines[i][0], -all_lines[i][1], all_lines[i][2], -all_lines[i][3])
        new_line_list.append(buffer_line)
    return new_line_list
```
Yielding simple patterns such as

![pattern2](images/kirigami.png)
Where all parameters such as any segment length proportionate to the cell size can be altered and adapted to the needs we might have. **It is a very interesting approach** as this can be used to create constructionist, optimized patterns and fractal patterns depending on what kind of design we are making. In any case it can be used as base to alter in any drawing applications.

### Example
Let's cut the pattern we just designed using the laser cutter. Of course, a few tests have to be made, first not really knowing how it would plan out, then improving what when wrong. The first try on cardboard would yield varying results. First the output power wasn't high enough, not cutting the cardboard properly. Finally the cut was fine, but the stress points where the squares have to turn were too rigid. Going back to the parametric design, we can change the space between lines right away, the final model works as intended but of course breaks off very easily. This kind of design better works on rubbery materials which I do not have access to at the moment.

<!-- blank line -->
<center>
<video controls="true" allowfullscreen="true" width = 300px>
  <source src="../videos/lasercutting.webm" type="video/webm">  
</video></center>
<!-- blank line -->

**note to embedding videos** : using ffmpg one can drastically reduce the video size for easier hosting. This barely watchable video doesn't deserve HD crisp 4K resolution anyways. However it shows pretty well what should be expected using a laser cutter. The following command changed the original video size from 260 Mbs ton 10Mbs. Note however that the preferred format for web is ```webm``` to ensure browser compatibility and lightness, getting down to 4 Mbs .

```bash
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
```
or

```bash
ffmpeg -i input.mp4 output.webm
```

![pattern2](images/lasercut1.png)

It is clear in this version that later tries must take the girth of the cut into account. This should be a lesson for all laser cutting in the future : not forgetting that it is substractive manufacturing that must be taken into account in the dimensioning.

## Basics of the vinyl cutter
We established previously we can't cut everything in the laser cutter. Especially if we want to cut sensitive to heat, fine and fragile materials. In that case the justified machine is the vinyl cutter, a very small controlled-by-computer blade allowing us to get very fine cuts and depth-control.

It is very easy to use as for the laser cutter : just vectorial pictures are sufficient. And as for the laser cutter a calibration run is more or less required to have a good idea of the depth settings to give depending on the materials. The vinyl cutter we tried is the Silouette Cameo, it's use is pretty straightforward even though it is a little regrettable that we are obligated to use proprietary software to use it.

From any vector format you want to cut, jump in into *Inkscape* to export the file in the ```.dxf``` format, which is rather standard for CAD work. Many other formats (even non-vector) will work, however only those keep track of the scaling which might be useful. Put in the paper material you wish to cut at the left arrow level, then with the guiding ring locked press hard on "load". You are ready to cut.

So I made a nice little logo, apparently we are supposed to put stickers on our laptops, its absolutely mandatory.


![pattern2](images/logopirate.png)
