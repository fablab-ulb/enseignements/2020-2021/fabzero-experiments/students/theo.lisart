# 5. Prototyping electronics : micro-controllers

To any modern conception, it is quite logical to approach also numerical electronics conception. Here let us do something easy to get acquainted with micro-controller programming, then the basis of soldering to make its own chip.

## Part 1 : Micro-controller programming
In the realm of manufactured programmable micro-controllers I call the king, the patriarch : Arduino. It's open-source and well known, well documented and thus often the default choice of programmable controller. In Fablab we have available the nifty beginner kit for it, which we are going to use to cover the basics. For simplicity's sake we'll just take one of the available projects in the kit, doing it while documenting what we learned, then add a sensor to control and get used to integer conversion in micro-controller.

<table><tr>
<td> <img src="../images/arduino1.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/arduino2.jpg" style="width: 300px;"/> </td>
</tr></table>

#### Setup of the arduino
[On the Arduino webpage](https://www.arduino.cc/en/Guide) we can find the startup guide and how to install the IDE for my operating system. There's no shame in following a guide, in fact it's the only smart thing to do. In Debian based Linux after downloading, un-taring, running the installation script in superuser mode you are now able to be welcomed in the IDE :

![](../images/arduinoide.png)

On this welcome page we can see two default main functions : the ```setup()``` function and the ```loop()``` function. As anyone can figure out for itself : the setup function is run when the chip is being powered on and the loop function is run at each processor clock tick. Let's see if we can make the IDE recognize the arduino by USB.

By connecting the arduino to USB, we can televerse a pseudo-C program to the arduino, for it to know what to do depending on pins volt levels. When working with micro-controllers we need to focus on two very important things from the microchip documentation : the flash memory we have, which is going to setup how long the code can be and how busy the data buss can be and the amount of pins available and their default use if there is one (ground pins, power sources, etc...). As expected, let us do a very simple thing to get into shape : blink of LED when a stress sensor gets too excited.

#### Making a small program : stress safety alarm

Let's work in two steps : blinking a led, then adding a conditional switch depending on a calibrated threshold relative to the working of a piezo-electic sensor. We'll not get into what this kind of crystal sensor does, but in all generalities it converts stress energy (forces, pressure, thus acceleration) into electric current. This is a widely used sensor thus not completely useless of a simple measuring setup.

The absolute essential thing to keep in mind in digital electronics is the way information is encoded in both paradigms : in analog electronics, where some pins of the arduino are dedicated to more on that later, we are only dealing with continuous expression of voltage levels. Usually for microelectronics it is between 0 and 5 volts. Data is treated as continuous signal and modified through feedback loops in operational amplifiers. This is a powerful tool, however most analog circuitry is build for one job and one job only, the one it was designed for in the first place, this is where numerical electronics comes into play. The interesting part about numerical electronics is first that using error correcting codes, data can be efficiently duplicated without too much of an error accumulation. In fact, it can be perfectly copied if we are careful enough. It is also **universal**, that is of course, that it can be re-programmed.

New technologies allows now for re-programming boards at the printed gate level, but let's not talk about this.

So to summarize, on the arduino we deal with analog/numerical input outputs with a programmable logic, flashed into the arduino's memory. This is essential as of course, all physical values will be analog values from a continuum, but things like LCD screens ask for a numerical output. Let's follow the beginner's course for blinking LED, then let's add some sensing. We will not have the time to go into for example using the LCD screen to display weight on the piezo for example. But how to do it if we wanted to will be explained.

##### Breadboarding, the first step of any electronics project

<table><tr>
<td> <img src="../images/breadboard.jpg" style="width: 300px;"/> </td>
<td> <img src="../images/arduino_circuit.jpg" style="width: 300px;"/> </td>
</tr></table>


This tiny breadboard will do the trick. You can easily build small circuit by connecting pins and micro-electronics component in parallel or in series. When looking at the picture, all vertical lines are connected together with a metal rod, this connecting components on the same column will connecting them in parallel, when connecting components on the horizontal line will connect them in series. For this project it really doesn't matter which jumper cable we use, however when it does get more complicated it's good practice to use the black male jumper cable as ground, and other colors for each programmed block.

when using a LED, always use a resistor to manage the current flowing through it. Never forget that a LED is a diode, thus has current limitations (hence a polarization as well, be careful not to break your entire brand new LED collection). [Full tutorial here](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink), at least for the blinking LED part.

Resistors have a color code, which can be checked for example [here](https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code).

Let's review first the code of the tutorial from Scott Fitzgerald in the Arduino documentation, then let's see how to modify it to add an analog control on the piezo-electric crystal.


```C
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

 [...]

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

So let's see what this code does. On ```setup()``` it initialize the function associated to a pin, firmware functions, here in the build we select ```LED_BUILTIN```, which is depending on the arduino model a LED on the board. We select also ```OUTPUT```, looking at the schematics of the arduino model the output of the buildin LED is associated to the ```digital pin 13```, this is then the pin we connect to the LED circuit in serie with only one other LED and one resistor.


In the ```loop()``` function we simply execute a write on the digital pin (either as ```HIGH``` or ```LOW```), on numerical pins there's only two binary state positions (of course). Depending on the board, high-voltage boards (5V) will report HIGH for any voltage on numerical mode superior to 3V, and on low-voltage board (3.3V), it will report HIGH in numerical mode for any superior to 2V. The ```delay()``` function waits the prescribed time in milli-seconds. Thus on the LED pin 13, we wait 1000 milli-seconds, set the voltage to 5V turning the LED on the circuit on, waiting 1000 milli-seconds again to finally starting again forever and ever (or until it gets bored, but no human has ever seen a computer get bored even though it does happen from time to time).


##### Improving
So the LED on the board is blinking, the LED on the breadboard is blinking, could we like, make it like, like more interesting... like ? Well a good start is how can we add a sensor, any of those to link it to for example, the blinking speed ? I have to my disposal a piezo-electric sensor so let's use just that.

A piezo-electric sensor has two output : the black one for the ground and the red one to be measured. Depending on force or pressure on the semiconductor, the difference between the two will increase.


```C

/*
  Blink it with force

  Turns on and off a LED depending on pressure applied on a piezo-electric sensor.
  Théo Lisart, Fabzero 2021
*/

// Variable declaration
int MIN_OSCILLATION_PERIOD = 20;  // In micro-second
int MAX_OSCILLATION_PERIOD = 2000;   // Default slow blinking
int MAXVOLT = 5;
int MINVOLT = 0;

int OSCILLATION_PERIOD = 2000;

int MAX_VOLT_TABLE = 1023;
int MIN_VOLT_TABLE = 0;

int sensorOutput = 3;            // Sensor connected to pin A3


int computePeriod(int analog_value) {

  /*
  Returns integer as blinking period for the LED
  analogRead(sensorOutput) --> Reads in 10 bits, thus from 0 to 1023 with 5Vols/steps.

  Linear translation from 0/stepvolts  -> 5000 milli-second break (slow-blink)
                        1023/stepvolts -> 20 milli-second break   (fast-blink)
  */


  int period = MAX_OSCILLATION_PERIOD - analog_value*(MAX_OSCILLATION_PERIOD - MAX_VOLT_TABLE)/MIN_OSCILLATION_PERIOD;
  return period;
}

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

  int readout = analogRead(sensorOutput);
  OSCILLATION_PERIOD = computePeriod(readout);

  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(OSCILLATION_PERIOD);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(OSCILLATION_PERIOD);                       // wait for a second
}
```

In this program we add the hability to read from an analog channel and how we can use conversions. Here we used a linear interpolation between encoding values from the 0 to 5 volts in integer steps from 0 to 1023. In our case this approach doesn't work well with the chosen crystal but the previous code works properly for example with some voltmeter. We can also for example, make the LED blink when the piezoelectric surface realize there's a hit on its surface, exciting isn't it ?

This wonderful application in action, not so interesting but it is however the basics for sensing and automation.

<!-- blank line -->
<video controls="true" allowfullscreen="true" width = 250px>
  <source src="../videos/electronics.webm" type="video/webm">  
</video>
<!-- blank line -->

##### What if I want to embed in into a very small volume ?
By making your own PCB, which is the subject of the next paragraph.
