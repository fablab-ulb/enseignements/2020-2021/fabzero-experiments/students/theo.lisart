# Fablab zero : Introduction

Welcome to the documentation page related to the FablabZero course at ULB. Feel free to get inspired by my mistakes and read through the creation process in frugal design using digital fabrication. This course is divided in modules which you can stroll through the sidebar. More information about the author on his [personal page](https://theolisart.github.io/) where you will be free to send either love letters or strongly worded disapproval messages.


# About me
The age was 7, the menacing finger of my grandfather pointed toward the piece of paper riddled with spelling mistakes convinced me I was not destined to be a failed novelist. Glad I caught that early, I can now focus on being a failed engineer and hopefully have a lot of fun in the process. The neat thing about the *devenir*, it's that it's all that matter. Gone where the days I could get away trying to electrolysis in my room risking death, or make cool explosions with toilet spray by saturating the liquid in test-tubes risking also death. We now have 3D printers, milling machines, and extremities to get too close from the laser cutter. Risking death is a part of life and science, but let's do it in the right order : first improving everyone's lives, then risking death on childish experiments.

Good design is a start [^1].


[^1]: Fight [hostile design](https://hostiledesign.org/)

# Final project
**The junkyard science project**, see Final Project in the navigation bar, the [Kitchen Microscope](https://gitlab.com/theo.lisart/the-kitchen-microscope) is the Proof of Work of the methodology I wanted to showcase and encourage. [You can download the poster's pdf here (french)](showcase_images/kitchen_microscope_mk2_2021.pdf).

![](showcase_images/showcase.jpg)

The restricted question that emerged in the scoping phase of this project is
**Can we create some useful and fun-to-build experimental setups, where the base ingredients are recycled materials ?**

I think after experimentation that the answer is yes, but needs quite some improvement.

<center>
<a href="https://gitlab.com/theo.lisart/the-kitchen-microscope">
   <img alt="Qries" src="other_images/gitlablogo.jpg"
   width=150" height="70">
</a>
</center>

If interested, more information about the project's concept and how it places itself in the [Sustainable development goals of the UN](https://sdgs.un.org/goals), see [here](Project/fabzero01.md).


<iframe width="640" height="360" src="https://www.youtube.com/embed/uy5eskTLvm4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<table><tr>
<td> <img src="showcase_images/blob1.jpg" style="width: 350;"/> </td>
<td> <img src="showcase_images/blob2.jpg" style="width: 350;"/> </td>
<td> <img src="showcase_images/fly.jpg" style="width: 350;"/> </td>
</tr></table></center>

<table><tr>
<td> <img src="showcase_images/hair.jpg" style="width: 350;"/> </td>
<td> <img src="showcase_images/worm.jpg" style="width: 350;"/> </td>
<td> <img src="showcase_images/feather.jpg" style="width: 350;"/> </td>
</tr></table></center>

<table><tr>
<td> <img src="showcase_images/glass1.jpg" style="width: 350;"/> </td>
<td> <img src="showcase_images/paper.jpg" style="width: 350;"/> </td>
</tr></table></center>

<table><tr>
<td> <img src="showcase_images/1.png" style="width: 350;"/> </td>
<td> <img src="showcase_images/2.png" style="width: 350;"/> </td>
<td> <img src="showcase_images/3.png" style="width: 350;"/> </td>
</tr></table></center>

![](showcase_images/weird.jpg)
![](showcase_images/salt.jpg)


All pieces for this first version :


![](Project/images/project/final/demont1.jpg)


# Sub-projects
* [Parametric flexlinks clones](https://gitlab.com/theo.lisart/parametric_flexlinks)
* [SVG kirigami parser](https://gitlab.com/theo.lisart/vector-kirigami-parser)
