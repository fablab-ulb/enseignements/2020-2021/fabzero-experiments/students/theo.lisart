# 3. The idea behind junkyard science
Based on the previous ideas, we can write the technical requirements for our junkyard microscope as well as somewhat of a branding. In the concept of junkyard science, the kitchen microscope must a whole idea which is complete. So let's build **some** branding.

## Quick branding - name, logo, idea
After a quick brainstorm with myself, I came up with some ideas of what the name of the project should be. The first that came to mind was ```Anyscope```, the microscope, made out of anything, really. But it sounded quite negative, so ended up choosing as a working name the ```Everyscope``` the microscope made out of anything (recycling), for everyone (the whole family : fun to build and easy to use).
