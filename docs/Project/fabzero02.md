# 2. Project management
One of the most interesting PM tool is the Gantt chart, is alows us to build consistent roadmaps by trying also to keep in mind that some tasks, to be done in parallel can have setbacks. The time which can be given to the project being rather small, control on important milestones. Today is the 15th of April, lets say we have five weeks to be conservative, Fablab only opens next week, which means that programming and conception should be our main focus as we do not have access to the machinery.

On Excel there's a lot of free [Gantt chart templates](https://templates.office.com/en-us/simple-gantt-chart-tm16400962) which of course we are going to use. At this point this chart only gives a rough idea of the production process. As example to keep in mind some conception time-management :


![image](images/ganttchart.png)

*(how do you like my company name ?)*

So the basic idea is to make overlaps and give a rough estimation of the needed time for each tasks. This kind of approach is very interesting when working with other people, but is useful when working alone also. In the fast iterative approach after establishing the ground work it is essential to see when we can manage to fit conception phases and building phases at the same time. On this first version of the chart (and not knowing exactly when the deadline is, just roughly at the end of the year). More explicit tasks overlapping will be displayed in the more refined chart that is updated during the whole conception process.

Up to now I gave myself around five weeks. Normally **the process will be only four**, this is to keep flexibility in time management. Once again this is only a personal guideline and a rough idea of the process to make sure (or at least, have the best idea) of the task at hand to have a finished product at the end. Many things... will go wrong.



# 3. Building the first idea : doing the most, with the least time

So in all horror one has to realize only four weeks are left before the end of the project, while writing the master thesis, while needing to study for my last exam session. Lesson in design : doing less. The only idea that stick somewhat to the first was **actually good junklab**, the main idea being quite clear : what about doing the best possible set of metrology equipment, or optical equipment from either the most recycled materials. In addition the project need to explain itself through design, hence building and making the project yourself need to explain how everything works. The most interesting idea would be building an insanely cheap spectrometer for material identification. The most motivated maker would want to make the most modular and complete set of measurement equipment and sensors. In a tool, make your own design corresponding to your needs, and ability to couple it to relatively available smartphone technology.

# 4. Proof of Concept (PoC) the junkyard microscope
The idea is then create a set of tools to make the most minimal lab equipment, which ideally has to work in the field and at the same time can be used as a learning tool. Thus has through any means explain itself. Ideally during this process we should document what works best to build variations and have some flexibility. Ideally, we will follow those very nifty steps :

1. Building the base principle prototype the fastest possible, with bits and bolds. First iteration isn't a design per say, more of a way to make it technically sound. The PoC of junkyard microscope has been proven and available. Where can we improve upon the concept which has been build the cheapest already ? The answer is making it amusing to use, and comfortable to use for an extended period of time.
2. Build the first "good" concept.
3. Create the printed optical schematics, improving it such as it "explain itself"
4. Improving for the jury, to make it a coherent whole.


As a start basic research brough to the idea that a sphere microscope using traditional western optics technique from the XVIIe century was a good start, and see if we can actually improve upon this idea to make an effective and useful tool for science and the communication of it.
