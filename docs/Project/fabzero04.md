# 4. Final Project : the kitchen microscope

<center>
<a href="https://gitlab.com/theo.lisart/the-kitchen-microscope">
   <img alt="Qries" src="../../other_images/gitlablogo.jpg"
   width=150" height="70">
 </a>
   </center>

## Basic physics, what exists, a little history of science
The basic idea behind building a ball lense microscope is of course not new, in actuality it is one of the oldest optical microscopy technique and many DIY tutorials are available on the Internet. Most of the handmade designs are based on making small cardboard holders by punching a small hole in the cardboard to hold the lense, then with a light source looking through the lense. Of course, as the lenses are so small this is a pain to use. Our aim here is to base the basics upon those techniques to build something a little easier to use with a smartphone camera, and at the same time have a little more control on the lense making even though we will assume we have not access to glasswork  tools. By glasswork tools we mean the most basic : bunzen burners and premade glass rods. Such projects can be found on youtube, but also great explanations on the [612photonics](https://612photonics.com/build-simple-smartphone-microscope-with-a-ball-lens/) website. They do not however give information on how to make the lense, which of course we will. Before anything like that, let's review some of the ground parameters of spherical lenses optics without proofs. Those results will help us later on for dimensionning.

### Basic ball lense parameters :
From any ball pen optics textbook or sources (for example [Edmond Optics](https://www.edmundoptics.eu/knowledge-center/application-notes/optics/understanding-ball-lenses/)), it is of course clear that only the ball diameter and materials are going to influence the two important optical parameters : the focusing length from the surface of the ball and the magnification ratio. By its geometrical nature, we define two focusing length : the **effective focus length (EFL)**, computed from one of the central plane of the lense and the **back focal length** (BFL). We write \(n   \) the refraction index associated to the material we are interested in. Those parameters are easily computed from classical ray optics, if we neglect spherical aberrations one has :

<center>
<table><tr>
<td> <img src="../images/ballEdmunt.png" style="width: 400px;"/> </td>
</tr></table></center>
<div style="text-align: right"> source : Edmont Optics</div>

Gives :

\[ EFL = \frac{nD}{4(n - 1)}, \]

\[ BFL = EFL - \frac{D}{2}\]

The two most used materials we are interested in are acrylic if you happened to put your hand on clear acrylic spheres and as an approximation for industrial glasses, \(SiO_2\). For the most physicists of us, we are clearly working in the paraxial limit for plane waves (left to the reader, starting by writing the propagator). We define the magnification as the ratio of the clear range distance and the focal length. This writes \(M = \frac{D_0}{f}\), in a purely spherical lens \(f = EFL\) which we know already. The *range of clear vision* which is set as standard at 250mm and in that case : \(M = \frac{250mm}{f}\)

<table><tr>
<td> <img src="../images/project/plots/balllense.png" style="width: 350;"/> </td>
<td> <img src="../images/project/plots/magn.png" style="width: 350;"/> </td>
</tr></table></center>

Where the orange line is the glass and blue line the acrylic line. This will give a good approximation of the focal distance for our ball lens microscope and have some idea predictions on the lens behavior. A 1mm lens then has a \( \approx \)x350 magnification but with a focal length a 1/4th of a mm. Hense as expected, the tighter the zoom, the harder it is going to be to focus precisely if the sample has to be almost stuck to the lens. A 3mm lens however will give \(\approx \)x240, with \(\approx\) 1mm focal distance, which is way easier to manage, but will probably be harder to make (see lens making section)

*Key parameters table for glass*

| D (mm)      | Magnification (Standard D0 = 250mm) |    BFL (mm) | EFL (mm) |
| ----------- | ------------- | ------------|----------|
| 1mm         |   342.1       | 0.2         | 0.7      |
| 1.2mm       |   285.1       | 0.3         | 0.9      |
| 1.4mm       |   244.4       | 0.3         | 1        |
| 1.8mm       |   190         | 0.4         | 1.3      |
| 2mm         |   171         | 0.5         | 1.4      |
| 2.2mm       |   155         | 0.5         | 1.6      |
| 2.4mm       |   142         | 0.55        | 1.7      |
| 2.6mm       |   131         | 0.6         |  2       |
| 2.8mm       |   122         | 0.65        | 2.1      |
| 3mm         |   114         | 0.7         |   2.2    |
| 3.2mm       |   106         | 0.75        |  2.3     |
| 3.4mm       |   101         | 0.8         |   2.5    |
| 3.8mm       |   90          | 0.9         |   2.7    |
| 4mm         |   85          | 0.95        |  2.9     |
| 4.4mm       |   78          | 1           |   3.2    |
| 4.8mm       |   71          | 1.1         |   3.5    |
| 5mm         |   68          | 1.15        |   3.6    |

Clearly, we need to be smart about the focusing system with distances that close to the lens. The obvious trade-off is the largest the lens, the less we have magnification real-estate but the more manageable the focusing distance is. Moreover, the largest the lens, the hardest to make. Less than 2mm lens will probably be unmanageable unless it is for somewhat of a quick look in the microscopic world by dropping samples directly on the lens and by moving the sample by hand hoping it ends up landing on a very clear spot. This quite important constraints imposes that we cannot slide the sample from small lenses as even the thickness of the glass will block the sample from being seen in the clear range.

## Prototyping
So, what is an optical microscope ? In a sense we can roughly split any such devices in three parts :

* The optics (magnifying lenses/ objective)
* The sample holder system
* The light source

In standard microscopes, the magnification and focusing can be done in the optical system, and the sample holder can have the advantage to have x-y movement. In making a ball lens it is quite clear from the obvious restricted field of view that the easiest path at this stage is implementing the focusing movement in z in the sample holder. The reason is that assuring alignment with the smartphone camera should be easy and least mobile possible (once again, very small lenses). As it seems that in most designs I could find of this approach most people were ordering small glass or acrylic spheres but very few were trying yo make them from scratch. In this project then, here's the do and don't of making small glass spheres in your kitchen. If either acrylic or glass spheres are available to you this first step is not necessary. We will try to find alternatives to the kitchen glasswork to broaden as much as possible the availability of the design.

### 1. Making kitchen lenses
As many people around the world, I never worked with glass. It then took some time to get used to the burning flames of hell (from gas stove), wearing glasses when working and actually caring about not burning the building down.

Needless to say the first runs were not successful. I really wanted to try on my own before reading and finding people's techniques for this specific task. But first I needed to learn a little about glass. I had clear memories from my industrial techniques course about the glass making process, and the numbers that stuck to my mind was 900, 1400 and 1800 °C, natural gas flames must be around that range so in my mind it was clear that it would work.

Well, under certain conditions. The first thing to remember about glass is that it is in fact a fluid with very high viscosity. When bringing to high temperature it only brings down that viscosity in a very non-linear way at activation temperature (glass-transition temperature \( T_g \). [A tutorial on how to make our lenses is available here](../Tutorials/kitchen_lenses.md), it takes around 20 and one hour to get a few good ones. The basic approach in this project is making a few of those and standardize the lens holder to 3D print. But first as I never worked with glass, let's try to go right at it. The protocol previously mentioned is the result of multiple failures I started just trying to sculpt a broken shard, of course without success. Thinking about glass, I tried to use gravity, but obviously the parts were droplet shaped. Now that I knew what were the problems with those techniques, I went to the internet and found some good examples, first the [museum of glass video](https://www.youtube.com/watch?v=2SJY0foypAo), then the absolute best video I could find from [Keeling Lab](https://www.youtube.com/watch?v=4o3Q2ueh6uI). The challenge is that they also uses glass rods, easy to manipulate instead of your average run of the mill pieces of glass made from sheer clumsiness. Their advice was to rub two pieces together under flame, then pull rapidly to make the glass straw. In my experience using my natural gas flame, this isn't the best approach. After a few runs, it was clear to me that the best approach would be to break shards as close as possible from industrial glass rods, which was sufficient for making the very small sphere we are interested in. The interesting part is that this approach would make the glass straws particularely thin due to the reduced surfaces. This makes harder the last step in making the spheres, but easier to cut off by shaking lightly to cleanly remove the glass sphere. Thus picking the shards is a very important step : thin enough to heat up properly, thick enough to produce a long enough straw, and thin enough again to have at the same time some weight and being detachable.

<table><tr>
<td> <img src="../images/project/lensehold/iterat_lens.jpg" style="width: 340px;"/> </td>
<td> <img src="../images/project/lensehold/perfect.jpg" style="width: 260px;"/> </td>
</tr></table>

By holding the ball lens in front of the smartphone camera or using a cardboard setup we can already test for the lenses quality. To my surprise the results are quite good, even in moderately good lenses. The good ones yield amazing results, but however have usually shorter forcusing length, which is a problem when trying to focus by hand (this is what we need to fix in the microscope design). Looking at tobacco that was left over (don't smoke folks, or do, I mean I am not your dad), pieces of plants and salt it is quite clear that the resolution of the camera as well as the clarity of the lens will be sufficient. This is a good sign, a good start.

<center>
<table><tr>
<td> <img src="../images/project/examples/breadmicr.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/project/examples/glassmicr.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/project/examples/tobaccomicr.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/project/examples/tobaccomicr2.jpg" style="width: 400px;"/> </td>
</tr></table></center>




### 2.1 First iteration the lense holder
I knew from the get go that I would want *at least* somewhat of a moduler-zoom system. In practice handling such small spheres is way too impractical in any case. The obvious approach would be 3D-print a tightening system, which is just the right thickness for the sphere to be optimally usable from both sides. Iterating on this idea, this forced me to actually improve the print quality of the printer I needed to calibrate for a while but didn't (I am not that much into modeling just prototyping). Once the printer was properly rebuild, I tried two kind of clipping systems : direct tightening or with external fittings. I settled with the direct, lego-style tightening for simplicity and effectiveness. Some round version failures are not included in the picture.

<table><tr>
<td> <img src="../images/project/lensehold/evol.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/project/lensehold/first_it.jpg" style="width: 400px;"/> </td>
</tr></table>


Setting for the simplest design, starting to print on glass and parametrically programming the diameter to be displayed on the piece. The final lens holder is set and evolved while working on the design showed in the next section.


<table><tr>
<td> <img src="../images/project/lensehold/final3.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/project/lensehold/final1.jpg" style="width: 400px;"/> </td>
</tr></table>


### 2.2 First iteration, over-engineering and giving up
The first and obvious approach would be taking the optical tower route : building around the optics we just presented. I really enjoy the *clipping* route where everything works progressively in tightening. So I designed the first aspects of the tower : passive x-y system with pushing vis (using material resistance as spring), vertical column on rails with gear system. While I was working on this system I realized how this approach was getting exponentially more complex. Adding to this a rail system for centering the lens on the phone, it wasn't optimal.

<center>
<table><tr>
<td> <img src="../images/project/firstmicro/showcase.jpg" style="width: 500px;"/> </td>
</tr></table></center>

Here we see the iterative process to make basic components. In the following the x-y structuren rail system and build first system. I never finished the design as it was obvious while I was making fitted gears to the vertical platform that it was over-engineering by a long shot.

<table><tr>
<td> <img src="../images/project/firstmicro/rail.jpg" style="width: 500px;"/> </td>
<td> <img src="../images/project/firstmicro/rail2.jpg" style="width: 500px;"/> </td>
</tr></table>
For the rail system and for the x-y stabilizing system :

<table><tr>
<td> <img src="../images/project/firstmicro/moving_xy.jpg" style="width: 500px;"/> </td>
<td> <img src="../images/project/firstmicro/buil.jpg" style="width: 500px;"/> </td>
</tr></table>

So I realized that I was on the wrong path. This was made clear for a few reasons :

1. Vertical z control with rails would not work on any phone, it has to be thought out relatively to the phone size, which is not acceptable.
2. This approach would be also impose building a custom platform, depending on the phone. The volume of the device exploding.
3. Volume increase due to the tower-approach.
4. Too many moving parts, hard to print, annoying to make.
5. Stirs too far from the initial design methodologies.

**This needs to be simplified. Scratch that, we go back to the drawing board.**

## 3. Going much simpler

### 3.1 Clamping system
Improving upon the very simple design made at [PNNL](https://www.pnnl.gov/available-technologies/pnnl-smartphone-microscope) based upon what we made previously is a way better idea than trying to think about every small pieces. The very simple clamp design is such a good idea there's no way we are not stealing it. How to improve it ? Well the annoying thing with their design is the fact that you need to change and reprint the entire apparatus to change the magnification, the focusing need to be done by hand approximately and the light source is not taken into account in the design.

<center><table><tr>
<td> <img src="../images/project/final/inspiration.jpg" style="width: 500px;"/> </td>
</tr></table></center>

Let's re-use our previous lens holder design and incorporate it in the clamp design. We'll improve the focusing once we can get good pictures from it. As there's _only_ tree pieces, let's go full parametric for the clamp.

So this time, we will build around the clamp and the optics, keeping the work we have done on the multiple-stage tightening system. On the second iteration I already have a very tight fit to the front-facing smartphone camera.  


Let's then print the PNNL design, the clamp design is simple but very effective. I quickly make my own version on a sine function in openscad and change the thickness of the clamp linearly with the distance. I can clip to that what will be in the future the focusing system which we are going to design in the next section. Iterating a few times to test for stability, facilitate alignement with the camera, and fitting tolerances we get :

<table><tr>
<td> <img src="../images/project/final/itclamp.jpg" style="width: 500px;"/> </td>
<td> <img src="../images/project/final/itclamp2.jpg" style="width: 500px;"/> </td>
</tr></table>

Those will be improved later on, the sample part is fitted on top, with (for now) a stabilization system, which actually makes use of the lens very comfortably.

<center>
<table><tr>
<td> <img src="../images/project/final/ite_clamp.jpg" style="width: 500px;"/> </td>
</tr></table></center>

So I am content with this idea for aligning camera and lens, it doesn't move and in *visioscope* mode it yields already alright performances, which will be improved upon after the focusing system. Looking at what we have, it is clear now that the system must be the most flexible possible such as it can be used with front-facing and back cameras. We will need to incorporate the foot into the clamping system to reduce the amount of moving and printable parts. This can probably be done by working on the sine frequencies and amplitudes making the clamp.

At this point as said previously we are getting our first results. Now the lens can be fixed on the phone without being hacky. Focusing already can be done easily by hand, for example on hair and feather :

<table><tr>
<td> <img src="../images/project/examples/hair.jpg" style="width: 422px;"/> </td>
<td> <img src="../images/project/examples/fether.jpg" style="width: 500px;"/> </td>
</tr></table>

The encouraging part is that due to the very short focusing length one could just place the sample on the lens and in visio mode can move around the sample as it wants in x-y, so no need for complicated movement. Let's try for the first time however to have some results with the most important sample : liquid.

Once again, the focusing will be done by hand, and why the videos are so shaky and hard to look at. Once again, we solve this in the next section. After getting some water from the pound in the parc, because in my apartment everything is dead (inside). For the first time, we see cute water ghosts. There's plenty of other things to see, but seeing them for the first time with those kitchen lenses is an awesome feeling.

<iframe width="640" height="360" src="https://www.youtube.com/embed/Xkx8AanqXQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The video is very compressed, demonstration videos will be shown in full glory when they won't be shaky anymore.

### 3.2 Reversible focusing system
As shown in the previous section, the modular fitted part on top of the system can be logically used for the moving part of the focusing system. From our previous mystakes it is quite clear that making a complicated gear system can be done in 3D printed parts, but however will be out of scope of the idea of this project. An idea would be a resistance rail system, but one has to be a little smart about it to reach without problem zones ultimately very close to the lens. In one of the available tutorials for a DIY type microscope the very tiny distances were dealt with by rotating the sample on a small platform. This is probably a good idea, making it more comfortable to use that way with the phone censor is a first step towards a functional prototype.

Using a pseudo-rail system and base for sample holder, the system is in a tight fit on the clamp and loose fit on the sampler holder. This allows to place the sample either on top or at the bottom of the base to use either on the front-facing camera (and profiting from surface tension of the droplet), or at the rear of the phone.

<table><tr>
<td> <img src="../images/project/system/focusing1.jpg" style="width: 350px;"/> </td>
<td> <img src="../images/project/system/focusing2.jpg" style="width: 350px;"/> </td>
</tr></table>

The holes on the square are for optional use of feet to stabilize the phone when used in front-facing camera.
The sample, is managed that way, on either sides :

![](images/project/system/sample.jpg)

All together, the system looks like this :

![](images/project/system/focus.jpg)

On my broken phone, the device holds :

<table><tr>
<td> <img src="../images/project/system/phone1.jpg" style="width: 350px;"/> </td>
<td> <img src="../images/project/system/phone2.jpg" style="width: 350px;"/> </td>
</tr></table>


And all the final pieces :

<table><tr>
<td> <img src="../images/project/final/demont1.jpg" style="width: 350px;"/> </td>
<td> <img src="../images/project/final/demont2.jpg" style="width: 350px;"/> </td>
</tr></table>

## Testing
In front-facing mode the system is very easy to use and focus, however of course the good phone censors are on the back cameras. The microscope is still very usable and yields good results, even though it is a little harder to handle when the sample cannot be seen. Better print should be done and micro-correcting versions. It should be iterated over quite a lot to be really good. In front-facing we get very good results on the middle-range lenses. It is particularly good at observing quasi-transparent (or not) micro-organisms from water samples. Here, gravity is our friend, as a lot of the interesting zoo and phyto-planctons come swimming (or drifting) close to the tip of the bubble.

A lot of absolutely disgusting tiny worms have been seen, which cannot be unseen. The performances are in fact better than what is shown in the showcase, mostly by a lack of time to take good shots. The ability to focus the microscope is however obvious in the following video. Where we follow the life of an invisible animal swimming far from our concerns. This will be the only over-compressed video, or we will not be seeing much of the little guy.

<iframe width="640" height="360" src="https://www.youtube.com/embed/uy5eskTLvm4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unfortunately the video is still somewhat compressed, but gives a good idea of what can be seen. Again, all files to make your own will be available shortly.

## Possible improvements
Many things should be done to improve this device : more customization to give it depth, more fitting optimization. Also the form of the clamp should be worked on to remove the need for an extra foot to hold the file right to use it comfortably, it is indeed an extra part to print which should be avoided for that kind of project. The overall design should also be optimized and cleaned. The two other obvious improvement are on the light control : this version has no embedded lights, which can obviously be problematic in practical situations. The latter however is not hard to fix, assuming for example that LED are available resources. If not, a more clever way to do it with the phone light should be considered and implemented directly into the camping system. Recycling old mirrors can be a good way to deal with this issue. Moreover the focusing system, which is exclusively manual should be kept that way, but helped with a flexible mechanism to be able to be either more precise, or at least block the system in position. This has to be investigated further.  

## On a broader aspect
Even though this project is at its first complete iteration, the philosophy behind it should be thought about in a broader way. At first to improve scientific gear accessibility, secondly to have more free and open-source designs of that idea. Large quantities of cheap optics and precision tools are not always available. Resilient designs, which are often in part with frugal designs, should be more encouraged for a large variety of reasons. For example, more complex physics applications can be thought about : absorption spectroscopy is achievable that way also especially since the camera sensors on phone cameras have gotten so good. Another idea, more complicated however, is for example Raman probes which also use ball lenses. The main challenges being building a laser cavity in that conditions (from available recycled materials) as well as optical filters and scattering materials. The initial step would be, if anyone would want to go that path, starting by finding a way to build beam spliters from scratch also from recycled goods.

So many possibilities, so little time.
[link to the Gilab project](https://gitlab.com/theo.lisart/the-kitchen-microscope)
