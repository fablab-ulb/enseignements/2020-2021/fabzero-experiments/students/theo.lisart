# 1. Introduction to FabZero : let's find a problem to solve

Once in a while one has to get to term with actually doing what is asked of oneself and admit defeat. One good look through the mirror to realize I will not -unfortunately- be 3D printing my one Qbit device or building a cool cyclotron even though I was giving false hope to the particles accelerator teacher that this time, the demonstration device will be build.

Coming to terms, and furthering the reflection on frugal design it is quite the time to actually make something useful, to my profound dread of course as to me, the most useless the science seems, the more interesting it seems to get.

So what is going on in the world which could be improved upon ? Well the most lunatic monkey with a relatively mediocre internet access could realize right away that it's not the problems that are lacking. One Discord message away from pandemic shenanigans or some Kurds I barely know complaining quite heavily that they are once again being shot at, by we don't really who this time, tough luck.

The lackluster engineer would simply say that the kurds merely need a bulletproof shielding system, the more open minded would say that the solution lies in finding solutions to not get shot at in the first place. It's a good start, but as for the cyclotron 5 ECTS are not enough to compete for a Nobel price, we'll have once again be seeing our ambitions reduced.

While they are not complaining about getting shot at, they are however complaining about a bunch of things that might be more manageable than international politics. One can directly see the advantage of frugal design when thinking about warzones, and the ideological compatibility with opensource and sharing knowledge in the autonomous regions of Syria makes it almost easy to think about. We can think about their two big initiatives : [Make Rojava Green Again](https://makerojavagreenagain.org/), or [Water for Rojava](https://mesopotamia.coop/water-for-rojava/) to name the most famous ones, organized in cooperatives in the context of the absolute ecological and humanitarian collapses usually brought by years of conflicts.

In that context four axes emerges :

- Civilian protection and communication, medical aid
- Ecological work
- Water access
- Inexpensive demonstration devices for schools

As water access is already worked on heavily in the ULB fablab it might be either an excellent idea to help and get involved, or a complete waste of time as I would not be able to bring anything interesting to the table. In a more egocentric way I also think I'd want my project to myself and bear the weight of failure on my own shoulders thank you very much.


## Prospective
Now that we established the ground work, the limiting factor will be the available tools. For each sector of needs we'll establish a list of ideas and reduce the search to what exists and what can be actually be build.
#### Axe 1 : civilian protection, communication and medical aid
Civilian short-range radars, human detection devices under rubble (thermal camera, microphone, CO_2 concentration detector), low-tech radio kit.
#### Axe 2 : Ecological work
Reliable small seeds refrigerated enclosure (seeds/vaccines doses that can be stored reliably in a backpack), greenhouse optimization device (passive greenhouse, removable sturdy greenhouse, smaller scale bag-size greenhouse, passive hydroponics, passive aeroponics). Opensource radiological metrology (lowtech geiger conter, dosimeter), cheap spectrometer.
#### Axe 3 : Water access
Water-quality testers ?
#### Axe 4 : Inexpensive demonstration devices
Cloud chambers (done in my past), to be researched.

# Problem based research : what do I want to make ?
In class the basic question and exchanges through small exercises is a gold mine to explore what works and what doesn't in the conception process. Emerging a well scoped project that is aiming to be at least at the surface useful is quite a violent intellectual process. I identified this question, which will seem totally useless at first :


**Can I create an object or tool, that self-explains itself and in the process can spark a sense of wonder in the user's mind ?**
From that quite a lot of things to extract, what do I mean when I say sense of wonder, what tools and to whom ? This question added to the previous shallow research yields a design philosophy with my previous constraints. Can I build an almost-toy, that is frugal, complicated enough to be useful and at the same time cheap, and accessible for the user to be instinctively willing to understand how it works and how it can be hacked into.

One idea would be find a sweet spot between the learning aspect, usefulness aspect and trying to have best results on simple devices. From my physics engineering background the kind of obvious choice is creating a **set of metrology equipment**, maybe in a reusable kit making a bunch of field and phone-powered measurement equipment. One can think especially of spectrometers, which are often cumbersome. Ideally, a set of modular liquid and solid analysis. This however will be for another time, as complexity in the basic concept is already too high for a first approach to the subject. However we can together think of a way to create comfortable measurement and microscopy instruments, where the critical part of the instrument is based upon recycled materials. The question thus evolves to

**Can we create some useful and fun-to-build experimental setups, where the base ingredients are recycled materials ?**

##UN sustainable goals
As help to scope the project, and having somewhat a way to justify this work one can easily reference the sustainable development goals of the UN (even though, I feel like this was more effective when it was restricted to 7...). In any case, the previous proposal is easily justified through those :

1. _Quality education_ : Quite obvious, easily accessible and cheap lab equipment, with available instructions to build and repair will be improving education.

2. _Sustainable cities_ : In the context of the fab-city, the ability to create its own gears quickly creates dynamic interactions and Free and Opensource philosophy.

3. _Responsible conception and consumption_ : Quite obvious from the previous one, moreover at the time of writing those lines, plans to make the microscope from recycled PLA is considered in the Fablab at ULB. This is good circular design.

4. _Clean water and sanitation_, _Climate action_ : Easily available water quality testing, rethinking production by decentralizing supply chain and recycling.
