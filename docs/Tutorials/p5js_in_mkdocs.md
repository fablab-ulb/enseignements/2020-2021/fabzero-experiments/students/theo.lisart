# How to implement P5.js in Mkdocs
[P5.js](https://p5js.org/) is one of the most popular javascript renderer out there for physics and processing animations, as well as small in-browser applications. It might be useful to incorporate into your documentation/dev journal to express some ideas clearly.


Two approaches are possible : either installing ```P5.js``` in your repo, calling the scripts in HTML directly into your ```.md``` files, or using the embedding functionality in the [P5.js online editor](https://editor.p5js.org/), exporting a small HTML script to embed the animation on your page.

In modern javascript development, we use CDN (content delivery networks) to avoid hosting libraries on each repositories.

## CDN for P5.js

Save your P5 script somewhere (i.e ```markdownroot/my_animation.js```), if you called your script ```my_animation.js``` you can call it right away on your markdown page :


``` html
<script src="https://cdn.jsdelivr.net/npm/p5@1.2.0/lib/p5.js"></script>
<script src="../my_animation.js"></script>
```

Or adding the following line to the javascript routines running in the whole website to your ```mkdocs.yml```, then calling the script :


``` python
extra_javascript:
  - https://cdn.jsdelivr.net/npm/p5@1.2.0/lib/p5.js
```




# Example [^1]:
<iframe style="width: 640px; height: 360px; overflow: hidden;"  scrolling="no" frameborder="0" src="https://editor.p5js.org/embed/B1j5yC2vQ"></iframe>


[^1]: Example Boid simulation courtesy of the [P5.js foundation](http://cmuems.com/2018/60212f/resources/embedding-p5js/)
