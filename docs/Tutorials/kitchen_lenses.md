# Homemade kitchen lenses : protocol

Let's go quickly over the good practice to make glass lenses in your kitchen and trying not to burn yourself or your house in the process. We assume you have at your disposal broken glass, a steel pince and another smaller cutting pince.

## Basic safety measures
* Especially for the bigger shards, glass if not uniformly heated will break. If the glass is of low quality with a lot of air bubbles trapped it can break quite spectacularly. It is important to note that it can somewhat explode also when cooling down. So when handling your hot glass it is better to have long sleeves and protective glasses on (or at least sunglasses like I did).

* Please be aware that metals are amazing heat conductors. At all time remember how hot the tools are getting and be mindful of separation between your hands and handles to the metal part of your tools. Or you will absolutely burn yourself.

* It is not dangerous, if you pay attention to what you are doing.

## Tools
* Usual electronics and line cutter pince
* Steel pince
* Natural gas stove
* Surface you don't care about to let your tools cool down

## Optimally broken shard
As we assumed that we do not have at our disposal machined glass rods usually available for glasswork, we have randomly broken shards. With your pince try to break the pieces of glass further into either pointy of parallelipiped shards, which are going to be easier to heat up uniformly.

## Making the lenses
1. Uniformly heat to red hot a small piece of glass
2. Pull quickly on both sides to make a long glass filament and let it cool
3. Cutting the filament
4. Plunging the filament at constant speed in the flame to make a glass droplet
Plunging at the right speed will make it spherical as the surface tension, plasma flux of the flame and gravity will cancel each other. This demands a little practice, but isn't in definitive hard to do.
5. Either being content with a perfect sphere with a straw to it, or trying to detach it while making it making it perfectly spherical.
6. If trying to detach it, when it reaches the right size strongly move up and down on the flame in one go. The lense will cool in its fall, which you can collect perfectly spherical on the stove when the heat is turned off.

<center>
<table><tr>
<td> <img src="../images/comparison.png" style="width: 400px;"/> </td>
</tr></table></center>

On this image you can see on the left a lense where the plunging was done too fast (the tip of the glass filament would go through the plasma of the flame and the glass droplet would have time to fall, making it droplet shape), or too slowly and the same way, the wind of the plasma and surface tension would not counter-balance gravity properly. On the right what you should have, the glass straw making a perfect fit into the sphere. You can then either cut it off, but will generate (somewhat acceptable) defaults. With a bit of luck, the detachment technique works and you'll get a perfect sphere.

<table><tr>
<td> <img src="../images/perfect.jpg" style="width: 400px;"/> </td>
<td> <img src="../images/hold.jpg" style="width: 400px;"/> </td>
</tr></table>
