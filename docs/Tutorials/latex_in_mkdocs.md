# How to implement LaTeX in Mkdocs

So you want to write gorgeous equations in your documentation ?

By default, \(\LaTeX \) is not supported by the Mkdocs markdown interpreter. One can easily fix this by following those 10 (it's only 3, but 10 sounds better) easy steps, this process is the same for all typesetting/layout elements using javascript plugins such as [Codeblocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/) using [Pygments](https://pygments.org/) :

## Prerequisites

* [Python extension Markdown](https://pypi.org/project/pymdown-extensions/)
Needed for most of the fancy typeset in Mkdocs, also useful for code blocks display which are needed in your technical projects anyways.

``` bash
pip install pymdown-extensions
```
(Don't forget to edit the ```requirements.txt``` [like so](CI_gitlab_packages.md))
## Install

* In your ```mkdocs.yml``` add the lines :

``` python
markdown_extensions:
  - pymdownx.arithmatex:
      generic: true
```

* Create additional javascript config at ```docs/javascripts/config.js``` in your root directory, those are the MathJax config for telling exactly how markdown should be compiled into \(\LaTeX \) output.


``` javascript
window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};
```


* **Alternatively**, you can directly edit the ```mkdocs.yml``` with javascript instructions by adding

```
extra_javascript:
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
```


## Use
Directly into your markdown pages :

* Text mode : ```\( your-latex-text \)```
* Math mode : ```\[ \yourLatexMath  \]```

I'll leave you with my personnal favorite equation perfectly typeset, the Lindblad equation of the density operator :


\[\frac{d\rho}{dt} = -\frac{i}{\hbar}[H, \rho] + \sum\gamma_n\bigg(L\rho L^{\dagger} - \frac{1}{2}\bigg\{L^{\dagger}L, \rho\bigg\}\bigg)   \]
### Ressources :

[Mathjax](https://squidfunk.github.io/mkdocs-material/reference/mathjax/), [Arithmatex](https://facelessuser.github.io/pymdown-extensions/extensions/arithmatex/)
