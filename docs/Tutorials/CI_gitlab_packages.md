# How to use external python packages with Gitlab CI (themes, and more)

So you found on the internet an awesome theme or some python packages you want to use with Mkdocs, but when you try installing them Gitlab yells at you saying they don't know what you are talking about ? It relentlessly tells you that your pipeline fails to propagate ? No worries, I got you covered.

## Requirements files and Gitlab compilation instructions

When using ```Mkdocs``` locally you simply installed additional packages with the ```pip``` command such that the python static generator knows what to do with your imports in the ```mkdocs.yml``` file. ```pip``` is in fact, a package manager for python which makes our lives easier (a little like ```apt``` is the Debian-based distros like Mate or Ubuntu, ```Pacman``` for Arch-based systems, all Linux flavors).

Under the hood, this is what Gitlab does when creating a pipeline : installing the packages required to generate the website, then building the websites.

When running a page job, Gitlab executes the following instructions :

```python
image: python:3.8-slim

before_script:
  - pip install -r requirements.txt

pages:
  script:
  - mkdocs build
  - mv _site public
  artifacts:
    paths:
    - public
  only:
    refs:
      - schedules
    changes:
      - "**/*.md"
```

For completeness let's see what this means lines per lines

1. ```image``` : selecting the python version it has to use
2. ```before_script``` : runs instructions like you do in your command line before asking anything from Mkdocs.
3. ```pages``` : tells Gitlab it is a page service, will then look for the ```.yml``` for further instructions. Then runs ```build```, creating the static site, ```mv``` is a standard UNIX command to move the ```_site``` folder into the hidden ```public``` folder, for the page service to display the produced webpage.

Following instructions are related to the schedule update to go through all markdown files. Let's go back to the ```before_script```, we have the line :

```bash
- pip install -r requirements.txt
```
Which translates to *install recursively (-r flag) python packages from the file ```requirements.txt```*

From this knowledge it is easy to realize that any properly named and hosted ```pip``` packages will be installed at deployment from the previous file. This can be interesting for many reasons, for example adding client-side functionalities which could be anything really. Looking at it, we have only two lines in the default pages :

```
mkdocs
mkdocs-bootswatch
```
So mkdocs, of course, and [mkdocs-bootswatch](https://github.com/mkdocs/mkdocs-bootswatch), which is the default theme packages which are proposed to you. Don't like them ? No worries, we'll add a new theme right now :

## Example : installing a new theme
Soooo you don't know CSS, it's ok, it's an entire career no need to feel bad about it. You still want a cool looking theme for your personal page and project journal and documentation. Cool thing is, most people like braging and sharing their cool themes. For the example sake let's take [Material](https://github.com/squidfunk/mkdocs-material), if you want to make your documentation/page startup slick looking (you might want your docuementation to look like a [2000' BIOS menu](https://gitlab.com/lramage/mkdocs-bootstrap386) I will not be the one stopping you).

Locally when working on our documentation we'll just follow the theme instructions :

```bash
pip install mkdocs-material
```
Then of course on the ```mkdocs.yml```

```python
theme:
  name: material
```

To translate to gitlab and for the repo-service to know how to handle the new theme, we add in the requirements file :

```
mkdocs
mkdocs-material
```

So Gitlab knows what to ```pip install``` at deployment. If you are not sure and will change your theme, keep ```mkdocs-bootswatch```, you can change all of this at any time.

There you go, easy right ?
